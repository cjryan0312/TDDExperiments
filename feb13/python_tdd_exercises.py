def reverse_list(lst):
    """
    Reverses order of elements in list lst.
    """
    backwards = []
    for item in lst:
        backwards = [item] + backwards
    return backwards

def reverse_string(s):
    """
    Reverses order of characters in string s.
    """
    reverse = ''
    for letter in s:
        reverse = letter + reverse
    return reverse

def is_english_vowel(c):
    """
    Returns True if c is an english vowel
    and False otherwise.
    """
    vowels = 'aeiouyAEIOUY'
    if c in vowels:
        return True
    else:
        return False

def count_num_vowels(s):
    """
    Returns the number of vowels in a string s.
    """
    vowels = 'aeiouyAEIOUY'
    counter = 0
    for i in s:
        if i in vowels:
            counter += 1
    return counter 

def histogram(l):
    """
    Converts a list of integers into a simple string histogram.
    """
    string = ''
    appending = '' 
    counter = 0
    for num in l:
        appending = num * '#'
        string += appending
        counter += 1
        if counter < len(l):
            string += '\n'
    return string

def get_word_lengths(s):
    """
    Returns a list of integers representing
    the word lengths in string s.
    """
    word = ''
    counter = 0
    word_lens = []
    for char in s:
        word += char
        if char == ' ':
            length = len(word) - 1
            word_lens.append(length)
            word = ''
        counter += 1
        if len(s) == counter: 
            length = len(word) 
            word_lens.append(length)
    return word_lens

def find_longest_word(s):
    """
    Returns the longest word in string s.
    In case there are several, return the first.
    """
    longList = get_word_lengths(s)
    def checkEqual(l):
        return l[1:] == l[:-1]
    special = checkEqual(longList)
    wordList = []
    word = ''
    counter = 0
    for char in s:
        word += char
        if char == ' ':
            if special:
                return word[:-1]
            wordList.append(word[:-1])
            word = ''
        counter += 1
        if len(s) == counter:
            wordList.append(word[:-1])
            word = ''
    greatestLength = max(longList)
    position = longList.index(greatestLength)
    return wordList[position]

def validate_dna(s):
    """
    Return True if the DNA string only contains characters
    a, c, t, or g (lower or uppercase). False otherwise.
    """
    letters = 'actgACTG'
    for char in s:
        if char not in letters:
            return False
    return True

def base_pair(c):
    """
    Return the corresponding character (lowercase)
    of the base pair. If the base is not recognized,
    return 'unknown'.
    """
    letters = 'actgACTG'
    special = validate_dna(c)
    if special == False:
        return 'unknown'
    pair = c.lower()
    if pair == 'a':
        return 't'
    elif pair == 't':
        return 'a'
    elif pair == 'c':
        return 'g'
    elif pair == 'g':
        return 'c'

def transcribe_dna_to_rna(s):
    """
    Return string s with each letter T replaced by U.
    Result is always uppercase.
    """
    rnaStrand = ''
    for letter in s:
        if letter == 'T' or letter == 't':
            rnaStrand += 'U'
        else:
            rnaStrand += letter
    rnaStrand = rnaStrand.upper()
    return rnaStrand

def get_complement(s):
    """
    Return the DNA complement in uppercase
    (A -> T, T-> A, C -> G, G-> C).
    """
    complementary = ''
    for letter in s:
        if letter == 'A' or letter == 'a':
            complementary += 'T'
        elif letter == 'T' or letter == 't':
            complementary += 'A'
        elif letter == 'C' or letter == 'c':
            complementary += 'G'
        elif letter == 'G' or letter == 'g':
            complementary += 'C'
    return complementary 

def get_reverse_complement(s):
    """
    Return the reverse complement of string s
    (complement reversed in order).
    """
    reverse = get_complement(s)
    complement = ''
    for letter in reverse:
        complement = letter + complement
    return complement 

def remove_substring(substring, string):
    """
    Returns string with all occurrences of substring removed.
    """
    final = ''
    for letter in string:
        final += letter 
        if substring in final:
            final = final[:-3]
    return final

def get_position_indices(triplet, dna):
    """
    Returns list of position indices for a specific triplet (3-mer)
    in a DNA sequence. We start counting from 0
    and jump by 3 characters from one position to the next.
    """
    dnaList = []
    positions = []
    dnaTrio = ''
    counter = 0
    for letter in dna:
        dnaTrio += letter
        if len(dnaTrio) == 3:
            dnaList.append(dnaTrio)
            dnaTrio = ''
    for item in dnaList:
        if item == triplet:
            positions.append(counter)
        counter += 1
    return positions

def get_3mer_usage_chart(s):
    """
    This routine implements a 'sliding window'
    and extracts all possible consecutive 3-mers.
    It counts how often they appear and returns
    a list of tuples with (name, occurrence).
    The list is alphabetically sorted by the name
    of the 3-mer.
    """
    tstList = []
    dnaList = []
    dnaList2 = []
    counter = 0
    trio = ''
    for letter in s:
        trio += letter
        if len(trio) == 3:
            dnaList.append(trio)
            dnaList2.append(trio)
            trio = ''
    for char in s[1:]:
        trio += char
        if len(trio) == 3:
            dnaList.append(trio)
            dnaList2.append(trio)
            trio = ''
    trio = ''
    for testing in s[2:]:
        trio += testing
        if len(trio) == 3:
            dnaList.append(trio)
            dnaList2.append(trio)
            trio = ''
    dnaList.sort()
    dnaList2.sort()
    for item in dnaList:
        if item in dnaList2:
            counter = dnaList.count(item)
            tstList.append((item, counter))
            while item in dnaList2:
                dnaList2.remove(item)
        elif item not in dnaList2:
            pass
    return tstList
        
print(get_3mer_usage_chart('CCGGAAGAGCTTACTTAGGAAGAA'))










