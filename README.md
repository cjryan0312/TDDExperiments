# Test Driven Devlpoment With Python
By: Christopher Ryan and Jack Newell

# About
Small experiments learning test driven development with python

# Presentations
In class we did a few presentations, these are the links to them

[Agile Software Development](https://docs.google.com/presentation/d/e/2PACX-1vTCuMaXePVoxtgOBVRzYJPH-ENbSbsIGF3o2SHONsxH99_6GYM7qtVoUPIgeTAYKh_CXgKA9D77h3Hk/pub?start=false&loop=false&delayms=3000)

[Regular Expressions](https://docs.google.com/presentation/d/e/2PACX-1vT43GyRSVtrWEtf4tPYEVp7OAjROkbtccllCK7ZGGneSctVUcMDWxqqa1qxCqKtHD9jF4ZZ7hnptGUt/pub?start=false&loop=false&delayms=3000)

[The Regex Polynomial Problem](https://docs.google.com/presentation/d/e/2PACX-1vR7WNj3fLrlJKUHWVvDnm8EKcniAEvcHjXVNkAeRb-zIBsoc2hz81OVwzr1KTDSahJ9DcELUsJbiufw/pub?start=false&loop=false&delayms=3000)






