import re
class Polynomial:
    def __init__(self, poly_str):
        self.poly_str = poly_str
        
    def __str__(self):
        return self.poly_str
    
    def extract_polynomial_values(self, poly_str):
        pattern = r"\s*([+-]?)(\d+)?\s*(x(\^(\d+))?)?"
        s = self.poly_str.replace(' ', '')
        group = re.findall(pattern, s)
        group.pop()
        coefExp = []
        for item in group:
            coef = 0
            exp = 0
            if item[1] is not '':
            coef = int(item[1])
            else:
                coef = 1
            if item[0] is '-':
                coef = -coef
            if item[3] is not '':
                exp = int(item[4])
            else:
                exp = 0
        coefExp += [(coef, exp)]
        return coefExp

def extract_polynomial_values(poly_str):
    #this is our regex s̶t̶o̶l̶e̶n̶ borrowed from another groups pattern
    pattern = r"\s*([+-]?)(\d+)?\s*(x(\^(\d+))?)?"
    #making a new string s that has no whitespace to make our lives easier
    s = poly_str.replace(' ', '')
    #puts each term into each singular group
    groups = re.findall(pattern, s)
    #removing last empty group
    groups.pop() 
    #empty list, will be used later
    returning = []
    for item in groups:
        #creating coefficient and exponent variables 
        coef = 0 
        exp = 0
        
        #if the second item(the coefficient) is not empty, then this runs
        if item[1] is not '':
            coef = int(item[1]) #coef is integer of of second item
        else:
            coef = 1 #if second item is empty, then coef is 1x, which is 1
        
        #if the first item is the minus symbol, the coefficient is negative
        if item[0] is '-':
            coef = -coef
        
        #if the fourth item(the exponent) is not empty, then code runs
        if item[3] is not '':
            exp = int(item[4]) #exponent is the fourth item
        else:
            exp = 0 #if there is no item then exponent is 0
        #adding the coeficcient and exponent as a total into final list
        returning += [(coef, exp)]

